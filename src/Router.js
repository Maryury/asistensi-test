import React from "react";
import { BrowserRouter as Router, Switch, Redirect } from "react-router-dom";

import PrivateRoute from "./components/PrivateRoute";
import Home from "./pages/home/Home";
import Flights from "./pages/filterFlights/Flights";

export default function RouterView() {
  return (
    <Router>
      <Switch>
        <PrivateRoute path="/" exact={true} component={Home} />
        <PrivateRoute path="/flight-serch" component={Flights} />
        <Redirect from="*" to="/" />
      </Switch>
    </Router>
  );
}
