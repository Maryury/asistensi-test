import { ctaConstants } from "../constansts/";

const initialState = {
  places: [],
  currencies: [],
  failure: [],
  quotes: [],
  error: false,
  success: false,
  countries: [],
  loading: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ctaConstants.LIST_PLACES:
      return {
        ...state,
        places: action.data
      };
    case ctaConstants.LIST_CURRENCIES:
      return {
        ...state,
        currencies: action.data
      };
    case ctaConstants.LIST_COUNTRIES:
      return {
        ...state,
        countries: action.data
      };
    case ctaConstants.BROWSE_QUOTES:
      return {
        ...state,
        quotes: action.data
      };
    case ctaConstants.LOADING_CTA:
      return {
        ...state,
        loading: true,
        success: false,
        error: false
      };
    case ctaConstants.SUCCESS_CTA:
      return {
        ...state,
        success: true,
        loading: false,
        error: false
      };
    case ctaConstants.FAILURE_CTA:
      return {
        ...state,
        success: false,
        loading: false,
        error: true
      };
    default:
      return state;
  }
}
