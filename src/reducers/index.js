import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import { reducer as form } from "redux-form";

import reducer from "./cta_reducers";

export default combineReducers({
  /* Reducers */
  reducer,
  /* Router Reducer */
  routerReducer,
  /* Redux Form */
  form
});
