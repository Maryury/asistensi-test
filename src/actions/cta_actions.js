import * as api from "../api/cta_api";
import { ctaConstants } from "../constansts";
import history from "../store/browserHistory";

export const getPlaces = data => {
  return { type: ctaConstants.LIST_PLACES, data };
};
export const getCurrencies = data => {
  return { type: ctaConstants.LIST_CURRENCIES, data };
};

export const getCountries = data => {
  return { type: ctaConstants.LIST_COUNTRIES, data };
};

export const getQuotes = data => {
  return { type: ctaConstants.BROWSE_QUOTES, data };
};

export const loading = () => {
  return { type: ctaConstants.LOADING_CTA };
};

export const success = () => {
  return { type: ctaConstants.SUCCESS_CTA };
};

export const failure = msg => {
  return { type: ctaConstants.FAILURE_CTA, msg };
};

export const fetchPlaces = code => {
  return (dispatch, getState) => {
    var countries = getState().reducer.countries;

    var search = countries.find(element => element.Code === code);

    var locale = "en-US";
    var currency = "USD";

    const request = api.listPlace(search, locale, currency);

    request
      .then(result => {
        if (result.status === 200) dispatch(getPlaces(result.data.Places));
      })
      .catch(error => {
        dispatch(failure(error));
      });
  };
};

export const fetchCountries = () => {
  return (dispatch, getState) => {
    const request = api.listCountries("en-US");

    request
      .then(result => {
        if (result.status === 200)
          dispatch(getCountries(result.data.Countries));
      })
      .catch(error => {
        dispatch(failure(error));
      });
  };
};

export const fetchCurriencies = () => {
  return (dispatch, getState) => {
    const request = api.listCurrencies();

    request
      .then(result => {
        if (result.status === 200)
          dispatch(getCurrencies(result.data.Currencies));
      })
      .catch(error => {
        dispatch(failure(error));
      });
  };
};

export const fetchQuotes = parms => {
  return (dispatch, getState) => {
    const request = api.browseQuotes(parms);

    dispatch(loading());
    request
      .then(result => {
        if (result.status === 200) {
          dispatch(getQuotes(result.data));
          dispatch(success());
          history.push("/flight-list");
        }
      })
      .catch(error => {
        dispatch(failure(error));
      });
  };
};
