import React from "react";
import { Route } from "react-router-dom";
import Container from "../container/Container";

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        return (
          <Container>
            <Component {...props} />
          </Container>
        );
      }}
    />
  );
};

export default PrivateRoute;
