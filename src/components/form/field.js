import React from "react";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { TextField } from "@material-ui/core";
import { blueGrey } from "@material-ui/core/colors";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: blueGrey[100]
    }
  }
});

export const renderFieldSelect = ({
  input,
  name,
  className,
  type,
  children,
  disabled,
  label,
  meta: { touched, error }
}) => {
  return (
    <form className="form-inline" noValidate>
      <ThemeProvider theme={theme}>
        <TextField
          margin="normal"
          select
          name={name}
          label={label}
          {...input}
          disabled={disabled}
          error={touched && error}
          helperText={touched && error && <div>{error}</div>}
          variant="outlined"
          style={{
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            width: 300
          }}
          InputLabelProps={{
            shrink: true
          }}
        >
          {children}
        </TextField>
      </ThemeProvider>
    </form>
  );
};

export const renderField = ({
  input,
  name,
  type,
  label,
  meta: { touched, error }
}) => {
  return (
    <form className="form-inline" noValidate>
      <ThemeProvider theme={theme}>
        <TextField
          margin="normal"
          {...input}
          label={label}
          type={type}
          name={name}
          error={touched && error}
          helperText={touched && error && <div>{error}</div>}
          variant="outlined"
          style={{
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            width: 300
          }}
          InputLabelProps={{
            shrink: true
          }}
        />
      </ThemeProvider>
    </form>
  );
};
