import moment from "moment";
import "moment/locale/es";
moment.locale("es");

export const validate = values => {
  const errors = {};
  if (!values.country) {
    errors.country = "Obligatorio";
  }
  if (!values.originplace) {
    errors.originplace = "Obligatorio";
  }
  if (!values.destinationplace) {
    errors.destinationplace = "Obligatorio";
  }
  if (!values.currency) {
    errors.currency = "Obligatorio";
  }
  if (!values.outboundpartialdate) {
    errors.outboundpartialdate = "Obligatorio";
  } else if (
    moment(values.outboundpartialdate).format("YYYY-MM-DD") <
    moment().format("YYYY-MM-DD")
  ) {
    errors.outboundpartialdate = "Día inválido";
  }
  if (!values.inboundpartialdate) {
    errors.inboundpartialdate = "Obligatorio";
  } else if (
    moment(values.inboundpartialdate).format("YYYY-MM-DD") <
    moment(values.outboundpartialdate).format("YYYY-MM-DD")
  ) {
    errors.inboundpartialdate = "Día inválido";
  }

  return errors;
};
