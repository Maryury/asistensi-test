import React from "react";
import Logo from "../assets/svg/Asistensi_logotipo.svg";

const Loading = ({ message }) => {
  return (
    <div
      style={{
        padding: 25,
        justifyContent: "center",
        alignItems: "center",
        color: "#fa4343"
      }}
    >
      <img src={Logo} alt="logo" style={{ width: 250, height: 200 }} />
      <h4>{message}</h4>
    </div>
  );
};

export default Loading;
