import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import LogoHeader from "../assets/svg/Logo";

export const links = [
  { name: "Inicio", to: "/" },
  { name: "Vuelos", to: "/flight-serch" },
  { name: "Planes", to: "/flight-list" },
  { name: "Beneficios", to: "/" },
  { name: "¿Quiénes somos?", to: "/" },
  { name: "Contacto", to: "/" }
];

const toggle = () => {
  let mainNav = document.getElementById("js-menu");

  mainNav.classList.toggle("active");
};

function Header() {
  return (
    <header>
      <nav className="navbar">
        <span className="navbar-toggle" id="js-navbar-toggle" onClick={toggle}>
          <FontAwesomeIcon icon={faBars} color={"#200844"} />
        </span>
        <Link className="logo" to={"/"}>
          <LogoHeader width={"50%"} height={"50%"} />
        </Link>
        <ul className="main-nav" id="js-menu">
          {links.map((link, i) => (
            <li key={i}>
              <Link className="nav-links" to={link.to}>
                {link.name}
              </Link>
            </li>
          ))}
        </ul>
      </nav>

      <div
        id="menu-toggle"
        className="menu-toggle"
        onClick={() => console.log("cambiar")}
      >
        <div className="hamburger"></div>
      </div>
    </header>
  );
}

export default Header;
