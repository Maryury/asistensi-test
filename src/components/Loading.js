import React, { useState } from "react";
import { connect } from "react-redux";
import { css } from "@emotion/core";
import { BeatLoader } from "react-spinners";
import Logo from "../assets/svg/Asistensi_logotipo.svg";

const override = css`
  display: block;
  margin: 0 auto;
`;

const Loading = ({ loading_cta }) => {
  const [loading] = useState(true);
  return (
    <div
      style={{
        padding: 25
      }}
    >
      <img src={Logo} alt="logo" style={{ width: 250, height: 200 }} />

      <BeatLoader css={override} color={"#fa4343"} loading={loading} />
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    loading_cta: state.reducer.loading
  };
};

export default connect(mapStateToProps)(Loading);
