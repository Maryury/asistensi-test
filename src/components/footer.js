import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTwitter,
  faFacebookF,
  faGooglePlusG,
  faInstagram
} from "@fortawesome/free-brands-svg-icons";
import LogoWhite from "../assets/svg/Logo-White";
import { links } from "./header";

function Footer() {
  return (
    <footer>
      <div>
        <ul>
          <LogoWhite width={"46%"} height={"100%"} />
        </ul>
        <ul className="ulTest">
          <li>
            <h4 className="title">Contactanos</h4>
          </li>
          <li>
            <p>Estados Unidos:</p>
            <a
              href="tel:+13054558811"
              rel="noopener noreferrer"
              className="color-pink"
            >
              +1 (305) 455.88.11
            </a>
          </li>
          <li>
            <p>Venezuela:</p>
            <a
              href="tel:+588008363637"
              rel="noopener noreferrer"
              className="color-pink no-display-xs "
            >
              +58 (800) 836.36.37 /
            </a>
            <a
              href="tel:+582128221260"
              rel="noopener noreferrer"
              className="color-pink"
            >
              +58 (212) 822.12.60
            </a>
          </li>
          <li>
            <p>España:</p>
            <a
              href="tel:+34911238276"
              rel="noopener noreferrer"
              className="color-pink"
            >
              +34 (911) 238.276
            </a>
          </li>
        </ul>
        <ul className="ulTestMenu" style={{ justifyContent: "center" }}>
          <li>
            <h4 className="title">Menu</h4>
          </li>
          {links.map((link, i) => (
            <li key={i}>
              <p>{link.name}</p>
            </li>
          ))}
        </ul>

        <ul style={{ justifyContent: "center", listStyle: "none " }}>
          <li>
            <h4 className="title">Siguenos</h4>
          </li>
          <br />
          <ul className="ulTest2">
            <li className="circleTest">
              <FontAwesomeIcon icon={faFacebookF} color="#11678D" size={"lg"} />
            </li>
            <li className="circleTest">
              <FontAwesomeIcon icon={faTwitter} size={"lg"} color="#11678D" />
            </li>
            <li className="circleTest">
              <FontAwesomeIcon
                icon={faGooglePlusG}
                size={"lg"}
                color="#11678D"
              />
            </li>
            <li className="circleTest">
              <FontAwesomeIcon icon={faInstagram} size={"lg"} color="#11678D" />
            </li>
          </ul>
        </ul>
      </div>
    </footer>
  );
}

export default Footer;
