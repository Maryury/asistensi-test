import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import "moment/locale/es";
moment.locale("es");

function FlightsPrice({ quotes }) {
  return (
    <div className="row2" style={{ padding: 20 }}>
      {quotes.Quotes.map((item, i) => {
        let symbol = quotes.Currencies.map(currency => currency.Symbol);

        let search = value => {
          return quotes.Places.find(element => element.PlaceId === value);
        };

        let carrier = quotes.Carriers.find(
          element => element.CarrierId === item.OutboundLeg.CarrierIds[0]
        );

        let origen = search(item.OutboundLeg.OriginId);
        let destination = search(item.OutboundLeg.DestinationId);

        return (
          <div className="column2" key={i}>
            <div className="card2">
              <h3>Precio: {`${item.MinPrice} ${symbol}`}</h3>
              <p>Origen: {origen.CityName}</p>
              <p>Destino: {destination.CityName}</p>
              <p>Transportista: {carrier.Name}</p>
              <p>
                Fecha de salida:{" "}
                {moment(item.OutboundLeg.DepartureDate).format("YYYY-MM-DD")}
              </p>
            </div>
          </div>
        );
      })}
    </div>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    quotes: state.reducer.quotes
  };
};

export default connect(mapStateToProps)(FlightsPrice);
