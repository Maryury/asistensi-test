import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import Loading from "../components/Loading";
import Message from "../components/Message";
import FlightsPrice from "./FlightsPrice";
import "moment/locale/es";
moment.locale("es");

function ListFlights({ quotes, success, loading, error, change }) {
  return (
    <>
      {loading && <Loading />}
      {success && quotes.Quotes && quotes.Quotes.length > 0 && <FlightsPrice />}
      {success && quotes.Quotes.length === 0 && (
        <Message message={"No hay vuelos disponibles"} />
      )}
      {error && <Message message={"Error: Intenta de nuevo"} />}
      <div onClick={change}>Volver</div>
    </>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    quotes: state.reducer.quotes,
    loading: state.reducer.loading,
    success: state.reducer.success,
    error: state.reducer.error
  };
};

export default connect(mapStateToProps)(ListFlights);
