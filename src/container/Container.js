import React from "react";
import Header from "../components/header";
import Footer from "../components/footer";

const Container = props => {
  return (
    <div className="App">
      <Header />
      <div className="main-panel">{props.children}</div>
      <Footer />
    </div>
  );
};

export default Container;
