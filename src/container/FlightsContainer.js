import React from "react";

function FlightsContainer({ children }) {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <div className="content mt--5">
        <div className="page-inner mt--3">
          <div className="justify-content-center" style={{ display: "flex" }}>
            <div className="card">{children}</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FlightsContainer;
