import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators, compose } from "redux";
import { Field, reduxForm, formValueSelector } from "redux-form";
import moment from "moment";
import MenuItem from "@material-ui/core/MenuItem";
import { renderField, renderFieldSelect } from "../../../components/form/field";
import { validate } from "../../../components/form/validate";
import { fetchPlaces, fetchQuotes } from "../../../actions/cta_actions";
import ListFlights from "../../ListFlights";
import "moment/locale/es";
moment.locale("es");

const inputs = [
  { label: "País", type: "select", name: "country" },
  { label: "Ciudad de origen", type: "select", name: "originplace" },
  { label: "Ciudad de destino", type: "select", name: "destinationplace" },
  { label: "Moneda", type: "select", name: "currency" },
  {
    label: "Fecha de partida",
    type: "date",
    min: true,
    name: "outboundpartialdate"
  },
  {
    label: "Fecha de regreso",
    type: "date",
    min: true,
    name: "inboundpartialdate"
  }
];

const FormFlights = ({
  handleSubmit,
  change,
  start_day,
  fetchPlaces,
  fetchQuotes,
  success,
  countries_array,
  country_value,
  currencies_array,
  places_array,
  destination_place,
  origin_place
}) => {
  const [view, setView] = useState(true);

  useEffect(() => {
    country_value && fetchPlaces(country_value);
  }, [country_value, fetchPlaces]);

  const onFormSubmit = values => {
    setView(false);
    fetchQuotes(values);
  };

  return (
    <>
      {view && (
        <>
          <div
            style={{
              padding: 15
            }}
          >
            <h2
              style={{
                color: "#200844",
                fontWeight: "bold",
                textAlign: "'left'"
              }}
            >
              ¡Elige tu destino y viaja ahora con los mejores precios!
            </h2>
          </div>
          <div className="row justify-content-center">
            {inputs.map((item, i) => (
              <Field
                key={i}
                name={item.name}
                component={
                  item.type === "select" ? renderFieldSelect : renderField
                }
                type={item.type}
                label={item.label}
                disabled={
                  (item.name === "originplace" ||
                    item.name === "destinationplace") &&
                  places_array.length === 0
                    ? true
                    : false
                }
              >
                {item.type === "select" &&
                (item.name === "originplace" ||
                  item.name === "destinationplace")
                  ? places_array
                      .sort((a, b) => a.PlaceName.localeCompare(b.PlaceName))
                      .map((option, i) => (
                        <MenuItem value={option.PlaceId} key={i}>
                          {option.PlaceName}
                        </MenuItem>
                      ))
                  : item.type === "select" && item.name === "currency"
                  ? currencies_array
                      .sort((a, b) => a.Code.localeCompare(b.Code))
                      .map((option, i) => (
                        <MenuItem value={option.Code} key={i}>
                          {option.Code}
                        </MenuItem>
                      ))
                  : item.type === "select" && item.name === "country"
                  ? countries_array
                      .sort((a, b) => a.Name.localeCompare(b.Name))
                      .map((option, i) => (
                        <MenuItem value={option.Code} key={i}>
                          {option.Name}
                        </MenuItem>
                      ))
                  : null}
              </Field>
            ))}
          </div>
          <div
            className="row justify-content-center"
            style={{ paddingTop: 12 }}
          >
            <button
              className="primary-button"
              type="submit"
              onClick={handleSubmit(onFormSubmit)}
            >
              Buscar
            </button>
          </div>
        </>
      )}
      {!view && <ListFlights change={() => setView(true)} />}
    </>
  );
};

const selector = formValueSelector("FormFlights");

const mapStateToProps = (state, ownProps) => {
  const start_day = selector(state, "outboundpartialdate");
  const country_value = selector(state, "country");
  const origin_place = selector(state, "originplace");
  const destination_place = selector(state, "destinationplace");

  return {
    start_day,
    country_value,
    origin_place,
    destination_place,
    countries_array: state.reducer.countries,
    currencies_array: state.reducer.currencies,
    places_array: state.reducer.places,
    success: state.reducer.success
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ fetchPlaces, fetchQuotes }, dispatch);

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  reduxForm({
    form: "FormFlights",
    validate
  })
)(FormFlights);
