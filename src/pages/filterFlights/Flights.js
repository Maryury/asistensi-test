import React, { useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchCountries, fetchCurriencies } from "../../actions/cta_actions";
import FlightsContainer from "../../container/FlightsContainer";
import FormFlights from "../../container/forms/filterFlights/FormFlights";
import "../../assets/css/container.css";

function Flights({
  fetchCountries,
  fetchCurriencies,
  countries_array,
  currencies_array
}) {
  useEffect(() => {
    if (countries_array.length === 0 || currencies_array.length === 0) {
      fetchCountries();
      fetchCurriencies();
    }
  });
  return (
    <FlightsContainer>
      <FormFlights />
    </FlightsContainer>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    countries_array: state.reducer.countries,
    currencies_array: state.reducer.currencies
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ fetchCountries, fetchCurriencies }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Flights);
