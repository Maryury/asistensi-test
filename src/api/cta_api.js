import axios from "axios";

const header = {
  "content-type": "application/octet-stream",
  "x-rapidapi-host": "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
  "x-rapidapi-key": "bf341702damsh941e73f9ee23483p1a2762jsndca55040072b",
  useQueryString: true
};

export const listPlace = (params, locale, currency) =>
  axios({
    method: "GET",
    url: `https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/autosuggest/v1.0/${params.Code}/${currency}/${locale}/`,
    headers: header,
    params: {
      query: params.Name
    }
  });

export const listCurrencies = () =>
  axios({
    method: "GET",
    url:
      "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/reference/v1.0/currencies",
    headers: header
  });

export const listCountries = locale =>
  axios({
    method: "GET",
    url: `https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/reference/v1.0/countries/${locale}`,
    headers: header
  });

export const browseQuotes = params =>
  axios({
    method: "GET",
    url: `https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browsequotes/v1.0/${params.country}/${params.currency}/${params.locale}/${params.originplace}/${params.destinationplace}/${params.outboundpartialdate}`,
    headers: header,
    params: {
      inboundpartialdate: params.inboundpartialdate
    }
  });
