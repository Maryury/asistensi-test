import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../reducers";
//import logger from "redux-logger";

const middleware = [thunk];

/*if (process.env.NODE_ENV === `development`) {
  const { logger } = require(`redux-logger`);

  middleware.push(logger);
}*/

export const store = createStore(
  rootReducer,
  compose(applyMiddleware(...middleware))
);
