## Asistensi Test

Una aplicación que se encargar de mostrar una lista de Vuelos, donde le permite al usuario filtrar la información por: País, Ciudad de origen, Ciudad de destino, Tipo de moneda para cancelar, la fecha de partida y la fecha de regreso.

## Estructura de carpetas

Después de la creación, su proyecto debería verse así:

```
asistensi-test/
  README.md
  node_modules/
  package.json
  yarn.lock
  public/
    index.html
  src/
    actions/
    api/
    assets/
      css/
      svg/
    components/
    constansts/
    container/
    pages/
      home/
      filterFlights/
    reducers/
    store/
    App.scss
    App.js
    App.test.js
    index.css
    index.js
    Router.js
```

Funciones de las carpetas o archivos:

- `public/index.html es la plantilla de página;`
- `src/index.js es el punto de entrada de JavaScript.`
- `src/actions/ en esta carpeta van todos los archivos que tenga acciones de redux.`
- `src/api/ en esta carpeta se crea un archivo que se encarga de las peticiones al servidor usando axios.`
- `src/assets/ en esta carpeta se encuentran los css y svg que se usan en la aplicación.`
- `src/components/ en esta carpeta se encuentran los componentes globales que se usan en la aplicación.`
- `src/constansts/ en esta carpeta se encuentran las constansts que se necesitan para el manejo de redux.`
- `src/container/ en esta carpeta se encuentran los formularios o contenedores que son usados en las vistas.`
- `src/pages/ en esta carpeta se encuentran los vistas o paginas principales de la aplicación.`
- `src/reducers/ en esta carpeta se encuentran las funciones con el reducer que almacena el estado de redux.`
- `src/store/ en esta carpeta se encuentran la configuración de la tienda redux.`
- `src/Router.js es el archivo donde se crea las rutas de la aplicación.`
- `src/App.js es el archivo donde se configura el provider de redux.`

Se puede decir que es una estructura por vistas o rutas, debido a que cada sección de nuestra aplicación va a tener una carpeta con su nombre, dando un mejor esquema del proyecto y entender que cada cosa tiene un lugar en específico.

## Instrucciones de instalación y configuración

1. Clone este repositorio.

### `Necesitará node e npm instalado globalmente en su máquina.`

2. Entra a la carpeta

### `cd asistensi-test`

3. Instala las dependencias:

### `npm install o yarn install`

4. Corre la aplicación:

### `npm start o yarn start`

5. Visita la aplicación

### `Abrir [http://localhost:3000](http://localhost:3000)`
